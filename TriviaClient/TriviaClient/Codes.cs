﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    enum Codes
    {
        // Response
        ERROR_RESPONSE_CODE = 101,
        LOGIN_RESPONSE_CODE = 150,
        SIGNUP_RESPONSE_CODE = 151,
        LOGOUT_RESPONSE_CODE = 152,
        GET_ROOMS_RESPONSE_CODE = 153,
        GET_PLAYER_IN_ROOM_RESPONSE_CODE = 154,
        GET_STATISTICS_RESPONSE_CODE = 155,
        GET_LEADERBOARD_RESPONSE_CODE = 156,
        JOIN_ROOM_RESPONSE_CODE = 157,
        CREATE_ROOM_RESPONSE_CODE = 158,

        // Request
        LOGIN_REQUEST_CODE = 160,
        SIGNUP_REQUEST_CODE = 161,
        LOGOUT_REQUEST_CODE = 162,
        GET_ROOMS_REQUEST_CODE = 163,
        GET_PLAYER_IN_ROOM_REQUEST_CODE = 164,
        GET_STATISTICS_REQUEST_CODE = 165,
        GET_LEADERBOARD_REQUEST_CODE = 166,
        JOIN_ROOM_REQUEST_CODE = 167,
        CREATE_ROOM_REQUEST_CODE = 168,

        // Other
        MAX_BUFFER_LEN = 1024
    }

}
