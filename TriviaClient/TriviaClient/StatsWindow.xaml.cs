﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for StatsWindow.xaml
    /// </summary>
    public partial class StatsWindow : Window
    {
        NetworkStream clientStream;
        string Username;

        public StatsWindow(NetworkStream clientstream, string username)
        {
            InitializeComponent();

            this.clientStream = clientstream;
            this.Username = username;

            List<byte> buffer = BitConverter.GetBytes(0).ToList<byte>();
            buffer.Insert(0, (byte)Codes.GET_STATISTICS_REQUEST_CODE);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();

            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                MessageBox.Show(errorResponse.message);
                this.Close();
            }
            else
            {
                Classes.GetStatisticsResponse getStatisticsResponse = BsonConverters.FromBson<Classes.GetStatisticsResponse>(serverData);
                List<string> stats = getStatisticsResponse.statistics;

                this.NumOfGames.Text = stats[2];
                this.NumOfCorrect.Text = stats[1];
                this.NumOfWrong.Text = stats[3];
                this.AverageTime.Text = stats[0];
                this.AverageTime.Text = this.AverageTime.Text.Substring(0, this.AverageTime.Text.Length - 4);
            }
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
