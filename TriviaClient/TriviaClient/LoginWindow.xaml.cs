﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    /// 
    public partial class LoginWindow : Window
    {
        NetworkStream clientStream;

        public LoginWindow(NetworkStream clientstream)
        {
            clientStream = clientstream;
            InitializeComponent();
            TextBoxes.Visibility = Visibility.Collapsed;
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            LoginMenu.Visibility = Visibility.Visible;
            TextBoxes.Visibility = Visibility.Collapsed;
            ErrorMessage.Text = "";
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            LoginMenu.Visibility = Visibility.Collapsed;
            TextBoxes.Visibility = Visibility.Visible;
            SignupSubmit.Visibility = Visibility.Collapsed;
            LoginSubmit.Visibility = Visibility.Visible;
            EmailTextBox.Visibility = Visibility.Collapsed;

            PasswordTextBox.Margin = new Thickness(0);
        }

        private void Signup_Click(object sender, RoutedEventArgs e)
        {
            LoginMenu.Visibility = Visibility.Collapsed;
            TextBoxes.Visibility = Visibility.Visible;
            LoginSubmit.Visibility = Visibility.Collapsed;
            SignupSubmit.Visibility = Visibility.Visible;
            EmailTextBox.Visibility = Visibility.Visible;

            PasswordTextBox.Margin = new Thickness(0,0,25,0);
        }

        private void LoginSubmit_Click(object sender, RoutedEventArgs e)
        {
            ErrorMessage.Text = "";
            Classes.LoginRequst loginRequst = new Classes.LoginRequst { username = UsernameTextBox.Text, password = PasswordTextBox.Text };
            List<byte> data = BsonConverters.ToBson<Classes.LoginRequst>(loginRequst);

            List<byte> buffer = BitConverter.GetBytes(data.Count()).ToList<byte>();
            buffer.Reverse();
            buffer.Insert(0, (byte)Codes.LOGIN_REQUEST_CODE);
            buffer.AddRange(data);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();


            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen]; 

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                ErrorMessage.Text = errorResponse.message;
            }
            else
            {
                Classes.LoginResponse loginResponse = BsonConverters.FromBson<Classes.LoginResponse>(serverData);
                MainWindow mainWindow = new MainWindow(this, clientStream, loginRequst.username);
                mainWindow.Show();
            }
        }

        private void SignupSubmit_Click(object sender, RoutedEventArgs e)
        {
            ErrorMessage.Text = "";
            Classes.SignupRequst signupRequst = new Classes.SignupRequst { username = UsernameTextBox.Text, password = PasswordTextBox.Text, email = EmailTextBox.Text };
            List<byte> data = BsonConverters.ToBson<Classes.SignupRequst>(signupRequst);

            List<byte> buffer = BitConverter.GetBytes(data.Count()).ToList<byte>();
            buffer.Reverse();
            buffer.Insert(0, (byte)Codes.SIGNUP_REQUEST_CODE);
            buffer.AddRange(data);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();


            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                ErrorMessage.Text = errorResponse.message;
            }
            else
            {
                Classes.SignupResponse signupResponse = BsonConverters.FromBson<Classes.SignupResponse>(serverData);
                MainWindow mainWindow = new MainWindow(this, clientStream, signupRequst.username);
                mainWindow.Show();
            }
        }
    }
}
