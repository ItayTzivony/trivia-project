﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    class Classes
    {
        public class RoomData
        {
            public uint id { get; set; }
            public string name { get; set; }
            public string maxPlayers { get; set; }
            public uint timePerQuestion { get; set; }
            public uint questionCount { get; set; }
            public bool isActive { get; set; }
        }

        //Error
        public class ErrorResponse
        {
            public string message { get; set; }
        }

        //Requsts
        public class LoginRequst
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        public class SignupRequst
        {
            public string username { get; set; }
            public string password { get; set; }
            public string email { get; set; }
        }

        public class GetPlayersInRoomRequest
        {
            public uint roomId { get; set; }
        }

        public class JoinRoomRequest
        {
            public uint roomId { get; set; }
        }

        public class CreateRoomRequest
        {
            public string roomName { get; set; }
            public uint maxUsers { get; set; }
            public uint questionCount { get; set; }
            public uint answerTimeout { get; set; }
        }

        //Responses
        public class LoginResponse
        {
            public uint status { get; set; }
        }

        public class SignupResponse
        {
            public uint status { get; set; }
        }

        public class LogoutResponse
        {
            public uint status { get; set; }
        }

        public class GetRoomsResponse
        {
            public uint status { get; set; }
            public List<RoomData> rooms { get; set; }
        }

        public class GetPlayersInRoomResponse
        {
            public List<string> players { get; set; }
        }

        public class GetStatisticsResponse
        {
            public uint status { get; set; }
            public List<string> statistics { get; set; } //[averageAnswerTime, numOfCorrectAnswers, numOfPlayerGames, numOfTotalAnswers]
        }

        public class JoinRoomResponse
        {
            public uint status { get; set; }
        }

        public class CreateRoomResponse
        {
            public uint status { get; set; }
        }

        public class GetLeaderboardResponse
        {
            public uint status { get; set; }
            public Dictionary<string, int> topThree { get; set; }
        }
    }
}
