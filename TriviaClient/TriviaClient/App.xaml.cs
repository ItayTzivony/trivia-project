﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Net;
using System.Net.Sockets;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        const string IP_ADDR = "127.0.0.1";
        const int PORT = 8519;
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(IP_ADDR), PORT);
            client.Connect(serverEndPoint);
            NetworkStream clientStream = client.GetStream();

            LoginWindow loginWindow = new LoginWindow(clientStream);
            loginWindow.ShowDialog();
            
        }
    }
}
