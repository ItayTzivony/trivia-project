﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for RoomWindow.xaml
    /// </summary>
    public partial class RoomWindow : Window
    {
        NetworkStream clientStream;
        string Username;
        int roomId;
        public RoomWindow(NetworkStream clientstream, string username, int roomID, string roomName, int maxPlayers, int questionCount, int timeForQuestion)
        {
            InitializeComponent();

            clientStream = clientstream;
            Username = username;
            roomId = roomID;

            RoomName.Text = roomName;
            RoomDetails.Text = String.Format("Max players: {0} | Question count: {1} | Time for each questions: {2}", maxPlayers, questionCount, timeForQuestion);

            Update_Players();
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            Update_Players();
        }

        private void Update_Players()
        {
            Classes.GetPlayersInRoomRequest getPlayersInRoom = new Classes.GetPlayersInRoomRequest { roomId = (uint)roomId }; //Sends a getPlayersInRoomRequest
            List<byte> data = BsonConverters.ToBson<Classes.GetPlayersInRoomRequest>(getPlayersInRoom);

            List<byte> buffer = BitConverter.GetBytes(data.Count()).ToList<byte>();
            buffer.Reverse();
            buffer.Insert(0, (byte)Codes.GET_PLAYER_IN_ROOM_REQUEST_CODE);
            buffer.AddRange(data);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();


            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                MessageBox.Show(errorResponse.message);
                this.Close();
            }
            else //Displays the players that are in the selected room
            {
                Classes.GetPlayersInRoomResponse getPlayersInRoomResponse = BsonConverters.FromBson<Classes.GetPlayersInRoomResponse>(serverData);
                List<string> players = getPlayersInRoomResponse.players;

                Players.Items.Clear();
                for (int i = 0; i < players.Count(); i++)
                {
                    Players.Items.Add(new ListBoxItem() { Content = players[i] });
                }
            }
        }
    }
}
