﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for JoinRoomWindow.xaml
    /// </summary>
    public partial class JoinRoomWindow : Window
    {
        NetworkStream clientStream;
        string Username;
        List<Classes.RoomData> roomsList = new List<Classes.RoomData>();

        public JoinRoomWindow(NetworkStream clientstream, string username)
        {
            clientStream = clientstream;
            Username = username;

            InitializeComponent();

            Refresh();
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void JoinRoomSend_Click(object sender, RoutedEventArgs e)
        {
            if (Rooms.SelectedItem != null)
            {
                uint selectedRoomId = (uint)((ListBoxItem)Rooms.SelectedItem).Tag;

                Classes.JoinRoomRequest joinRoomRequest = new Classes.JoinRoomRequest { roomId = selectedRoomId }; //Sends a joinRoomRequest
                List<byte> data = BsonConverters.ToBson<Classes.JoinRoomRequest>(joinRoomRequest);

                List<byte> buffer = BitConverter.GetBytes(data.Count()).ToList<byte>();
                buffer.Reverse();
                buffer.Insert(0, (byte)Codes.JOIN_ROOM_REQUEST_CODE);
                buffer.AddRange(data);

                clientStream.Write(buffer.ToArray(), 0, buffer.Count());
                clientStream.Flush();


                byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

                clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

                int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
                byte[] serverData = new byte[serverDataLen];

                Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

                if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
                {
                    Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                    MessageBox.Show(errorResponse.message);
                    Close();
                }
                else //Joins the user to the selected room
                {
                    Classes.JoinRoomResponse joinRoomResponse = BsonConverters.FromBson<Classes.JoinRoomResponse>(serverData);

                    this.Close();
                    Classes.RoomData roomData = roomsList[Rooms.SelectedIndex];
                    RoomWindow roomWindow = new RoomWindow(clientStream, Username, (int)selectedRoomId, roomData.name, Int32.Parse(roomData.maxPlayers), (int)(roomData.questionCount), (int)(roomData.timePerQuestion));
                    roomWindow.ShowDialog();
                }
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void Refresh()
        {
            List<byte> buffer = BitConverter.GetBytes(0).ToList<byte>(); //Sends a getRoomsRequest
            buffer.Insert(0, (byte)Codes.GET_ROOMS_REQUEST_CODE);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();


            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                MessageBox.Show(errorResponse.message);
                Close();
            }
            else
            {
                Classes.GetRoomsResponse getRoomsResponse = BsonConverters.FromBson<Classes.GetRoomsResponse>(serverData);
                if (getRoomsResponse != null)
                {
                    roomsList = getRoomsResponse.rooms;
                }
            }
            Rooms.Items.Clear();
            for (int i = 0; i < roomsList.Count(); i++) //Displays the active rooms
            {
                Rooms.Items.Add(new ListBoxItem() { Content = roomsList[i].name, Tag = roomsList[i].id });
            }
        }

        private void RoomSelected(object sender, SelectionChangedEventArgs e)
        {
            uint selectedRoomId = (uint)((ListBoxItem)Rooms.SelectedItem).Tag; //Gets the tag of the currently selected room

            Classes.GetPlayersInRoomRequest getPlayersInRoom = new Classes.GetPlayersInRoomRequest { roomId = (uint)((ListBoxItem)Rooms.SelectedItem).Tag }; //Sends a getPlayersInRoomRequest
            List<byte> data = BsonConverters.ToBson<Classes.GetPlayersInRoomRequest>(getPlayersInRoom);

            List<byte> buffer = BitConverter.GetBytes(data.Count()).ToList<byte>();
            buffer.Reverse();
            buffer.Insert(0, (byte)Codes.GET_PLAYER_IN_ROOM_REQUEST_CODE);
            buffer.AddRange(data);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();


            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                MessageBox.Show(errorResponse.message);
                this.Close();
            }
            else //Displays the players that are in the selected room
            {
                Classes.GetPlayersInRoomResponse getPlayersInRoomResponse = BsonConverters.FromBson<Classes.GetPlayersInRoomResponse>(serverData);
                List<string> players = getPlayersInRoomResponse.players;

                Players.Items.Clear();

                for (int i = 0; i < players.Count(); i++)
                {
                    Players.Items.Add(new ListBoxItem() { Content = players[i] });
                }
            }
        }
    }
}
