﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NetworkStream clientStream;
        string Username;
        
        public MainWindow(Window previousWindow, NetworkStream clientstream, string username)
        {
            previousWindow.Close();
            clientStream = clientstream;
            Username = username;
            InitializeComponent();
            WelcomeBox.Text = "Welcome " + username + "!";

            List<byte> buffer = BitConverter.GetBytes(0).ToList<byte>();
            buffer.Insert(0, (byte)Codes.GET_LEADERBOARD_REQUEST_CODE);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();

            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                MessageBox.Show(errorResponse.message);
                this.Close();
            }
            else
            {
                Classes.GetLeaderboardResponse getLeaderboardResponse = BsonConverters.FromBson<Classes.GetLeaderboardResponse>(serverData);
                if (getLeaderboardResponse.topThree.Count > 0)
                {
                    User1Stats.Text = getLeaderboardResponse.topThree.ElementAt(0).Key + "(" + getLeaderboardResponse.topThree.ElementAt(0).Value.ToString() + ")";
                }
                if (getLeaderboardResponse.topThree.Count > 1)
                {
                    User2Stats.Text = getLeaderboardResponse.topThree.ElementAt(1).Key + "(" + getLeaderboardResponse.topThree.ElementAt(1).Value.ToString() + ")";
                }
                if (getLeaderboardResponse.topThree.Count > 2)
                {
                    User3Stats.Text = getLeaderboardResponse.topThree.ElementAt(2).Key + "(" + getLeaderboardResponse.topThree.ElementAt(2).Value.ToString() + ")";
                }
            }
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow joinRoomWindow = new JoinRoomWindow(clientStream, Username);
            joinRoomWindow.ShowDialog();
        }

        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow createRoomWindow = new CreateRoomWindow(clientStream, Username);
            createRoomWindow.ShowDialog();
        }

        private void MyStatistics_Click(object sender, RoutedEventArgs e)
        {
            StatsWindow statsWindow = new StatsWindow(clientStream, Username);
            statsWindow.ShowDialog();
        }
    }
}
