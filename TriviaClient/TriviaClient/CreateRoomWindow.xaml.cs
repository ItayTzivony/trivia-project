﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for CreateRoomWindow.xaml
    /// </summary>
    public partial class CreateRoomWindow : Window
    {
        NetworkStream clientStream;
        string Username;

        public CreateRoomWindow(NetworkStream clientstream, string username)
        {
            clientStream = clientstream;
            Username = username;
            InitializeComponent();
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CreateRoomSend_Click(object sender, RoutedEventArgs e)
        {
            UInt32 maxUsersTemp = 0, questionCountTemp = 0, answerTimeoutTemp = 0;
            if (!(UInt32.TryParse(NumOfPlayersTextBox.Text, out maxUsersTemp) && UInt32.TryParse(NumOfQuestionsTextBox.Text, out questionCountTemp) && UInt32.TryParse(TimeForQuestionsTextBox.Text, out answerTimeoutTemp)))
            {
                ErrorMessage.Text = "ERROR: One or more parameters invalid";
                return;
            }
            Classes.CreateRoomRequest createRoomRequest = new Classes.CreateRoomRequest { roomName = RoomNameTextBox.Text, maxUsers = maxUsersTemp, questionCount = questionCountTemp, answerTimeout = answerTimeoutTemp };
            List<byte> data = BsonConverters.ToBson<Classes.CreateRoomRequest>(createRoomRequest);

            List<byte> buffer = BitConverter.GetBytes(data.Count()).ToList<byte>();
            buffer.Reverse();
            buffer.Insert(0, (byte)Codes.CREATE_ROOM_REQUEST_CODE);
            buffer.AddRange(data);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();


            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                ErrorMessage.Text = errorResponse.message;
            }
            else //Joins the creator of the room to the newly created room
            {
                Classes.CreateRoomResponse createRoomResponse = BsonConverters.FromBson<Classes.CreateRoomResponse>(serverData);
                //JoinCreatorToRoom(RoomNameTextBox.Text, maxUsersTemp, questionCountTemp, answerTimeoutTemp);
                string roomName = RoomNameTextBox.Text;
                uint roomId = GetIdByName(roomName);
                this.Close();
                RoomWindow roomWindow = new RoomWindow(clientStream, Username, (int)roomId, roomName, (int)maxUsersTemp, (int)questionCountTemp, (int)answerTimeoutTemp);
                roomWindow.ShowDialog();
            }
            this.Close();
        }

        private void JoinCreatorToRoom(string roomName, uint maxUsers, uint numOfQuestions, uint answerTimeout)
        {
            uint roomId = GetIdByName(roomName);

            Classes.JoinRoomRequest joinRoomRequest = new Classes.JoinRoomRequest { roomId = roomId }; //Sends a joinRoomRequest
            List<byte> data = BsonConverters.ToBson<Classes.JoinRoomRequest>(joinRoomRequest);

            List<byte> buffer = BitConverter.GetBytes(data.Count()).ToList<byte>();
            buffer.Reverse();
            buffer.Insert(0, (byte)Codes.JOIN_ROOM_REQUEST_CODE);
            buffer.AddRange(data);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();


            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                MessageBox.Show(errorResponse.message);
                Close();
            }
            else //Joins the user to the selected room, and opens the room window
            {
                Classes.JoinRoomResponse joinRoomResponse = BsonConverters.FromBson<Classes.JoinRoomResponse>(serverData);

                this.Close();
                RoomWindow roomWindow = new RoomWindow(clientStream, Username, (int)roomId, roomName, (int)maxUsers, (int)numOfQuestions, (int)answerTimeout);
                roomWindow.ShowDialog();
            }
        }

        private uint GetIdByName(string roomName)
        {
            List<byte> buffer = BitConverter.GetBytes(0).ToList<byte>(); //Sends a getRoomsRequest
            buffer.Insert(0, (byte)Codes.GET_ROOMS_REQUEST_CODE);

            clientStream.Write(buffer.ToArray(), 0, buffer.Count());
            clientStream.Flush();


            byte[] serverBuffer = new byte[(int)Codes.MAX_BUFFER_LEN];

            clientStream.Read(serverBuffer, 0, (int)Codes.MAX_BUFFER_LEN);

            int serverDataLen = BitConverter.ToInt32(serverBuffer, 5);
            byte[] serverData = new byte[serverDataLen];

            Array.Copy(serverBuffer, 5, serverData, 0, serverDataLen); //Copies the bson ONLY

            if ((Codes)serverBuffer[0] == Codes.ERROR_RESPONSE_CODE)
            {
                Classes.ErrorResponse errorResponse = BsonConverters.FromBson<Classes.ErrorResponse>(serverData);
                MessageBox.Show(errorResponse.message);
                Close();
            }
            else
            {
                Classes.GetRoomsResponse getRoomsResponse = BsonConverters.FromBson<Classes.GetRoomsResponse>(serverData);
                List<Classes.RoomData> rooms = getRoomsResponse.rooms;
                for(int i=0; i<rooms.Count; i++)
                {
                    if(rooms[i].name == roomName)
                    {
                        return rooms[i].id;
                    }
                }
            }
            return 0;
        }
    }
}
