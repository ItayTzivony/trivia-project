#pragma once
#include "WSAInitializer.h"
#include <Windows.h>
#include "IDatabase.h"
#include <iostream>
#include <string>
#include <vector>
#include <map>

#define DB_NAME "triviaDatabase"
#define COMMAND_LEN 100

class SqliteDatabase : public IDatabase
{
private:
	sqlite3* _database;

	bool open();
	void close();

public:
	SqliteDatabase();
	~SqliteDatabase();

	bool doesUserExist(std::string username);
	bool doesPasswordMatch(std::string password, std::string username);
	void addNewUser(std::string username, std::string password, std::string email);

	std::vector<std::string> getQuestions(int amount);
	float getPlayerAverageAnswerTime(std::string username);
	int getNumOfCorrectAnswers(std::string username);
	int getNumOfTotalAnswers(std::string username);
	int getNumOfPlayerGames(std::string username);
	int getPlayerRank(std::string username);
	int getPlayerExperience(std::string username);
	std::map<std::string, int> getLeaderBoard();
};
