#pragma once

#include "Room.h"
#include <map>

class RoomManager
{
private:
	std::map<int, Room> m_rooms;
public:
	RoomManager();
	~RoomManager();
	void createRoom(LoggedUser loggedUser, RoomData roomData);
	void deleteRoom(int ID);
	bool getRoomState(int ID);
	std::map<int, Room>& getRooms();
};