#pragma once
#include "IRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "Helper.h"
#include "Codes.h"
#include "MenuRequestHandler.h"
#include <iostream>

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* m_handlerFactory;
public:
	LoginRequestHandler();
	LoginRequestHandler(RequestHandlerFactory& requestHandle);
	~LoginRequestHandler();

	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handleRequest(RequestInfo requestInfo);
	RequestResult login(RequestInfo requestInfo);
	RequestResult signup(RequestInfo requestInfo);
};
