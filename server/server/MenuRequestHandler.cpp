#include "MenuRequestHandler.h"
#include "RequestHandlerFactory.h"

MenuRequestHandler::MenuRequestHandler()
{
	this->m_handlerFactory = new RequestHandlerFactory();
}

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory& requestHandle) : m_handlerFactory(&requestHandle)
{
}

MenuRequestHandler::~MenuRequestHandler()
{
	delete this->m_handlerFactory;
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	int code = requestInfo.reqeustID;

	return code >= LOGOUT_REQUEST_CODE && code <= CREATE_ROOM_REQUEST_CODE;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestResult;
	std::vector<unsigned char> buffer;
	int code = requestInfo.reqeustID;

	if (!isRequestRelevant(requestInfo))
	{
		ErrorResponse errorResponse = { "Non-existing request code" };
		buffer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.newHandler = nullptr;
	}
	else if (code == CREATE_ROOM_REQUEST_CODE) //Creates a room
	{
		requestResult = createRoom(requestInfo);
	}
	else if (code == JOIN_ROOM_REQUEST_CODE) //Joins a room
	{
		requestResult = joinRoom(requestInfo);
	}
	else if (code == GET_STATISTICS_REQUEST_CODE) //Gets the statistics of a user
	{
		requestResult = getStatistics(requestInfo);
	}
	else if (code == GET_LEADERBOARD_REQUEST_CODE) //Gets the leaderboard
	{
		requestResult = getLeaderboard(requestInfo);
	}
	else if (code == GET_PLAYER_IN_ROOM_REQUEST_CODE) //Gets the players in a specific room
	{
		requestResult = getPlayersInRoom(requestInfo);
	}
	else if (code == GET_ROOMS_REQUEST_CODE) //Gets all the active rooms
	{
		requestResult = getRooms(requestInfo);
	}
	else if (code == LOGOUT_REQUEST_CODE) //Logouts a specific user
	{
		requestResult = signout(requestInfo);
	}

	return requestResult;
}

RequestResult MenuRequestHandler::signout(RequestInfo requestInfo)
{
	RequestResult requestResult;

	std::string username;

	json jsonTranslated = json::from_bson(requestInfo.buffer); //Transforms the buffer's BSON format into a JSON format object
	username = jsonTranslated["username"]; //Takes the username string from the JSON object

	if (m_handlerFactory->getLoginManager().logout(username)) //Successful signout
	{
		LogoutResponse logoutResponse = { 1 };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(logoutResponse);
		requestResult.newHandler = this;
	}
	else
	{
		ErrorResponse errorResponse = { "Non-existing username" };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.newHandler = nullptr;
	}

	return requestResult;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo requestInfo)
{
	RequestResult requestResult;

	std::map<int, Room>& rooms = m_handlerFactory->getRoomManager().getRooms();
	std::vector<RoomData> roomsData;

	for (auto it = rooms.begin(); it != rooms.end(); ++it) //Fills the roomsData vector with the data from the rooms vector
	{
		roomsData.push_back(it->second.getRoomData());
	}

	GetRoomsResponse getRoomsResponse = { 1 };
	getRoomsResponse.rooms = roomsData;
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(getRoomsResponse);
	requestResult.newHandler = this;

	return requestResult;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo requestInfo)
{
	RequestResult requestResult;

	int roomID;
	bool roomExists = false;

	json jsonTranslated = json::from_bson(requestInfo.buffer); //Transforms the buffer's BSON format into a JSON format object
	roomID = jsonTranslated["roomId"]; //Takes the roomID string from the JSON object

	std::map<int, Room>& rooms = m_handlerFactory->getRoomManager().getRooms();
	std::vector<LoggedUser> players;
	std::vector<std::string> playerNames;

	if (rooms.find(roomID) != rooms.end())
	{
		players = rooms[roomID].getAllUsers();
		roomExists = true;
	}


	if (roomExists && players.size() > 0)
	{
		for (int i = 0; i < players.size(); i++) //Fills the playerNames vector with the names of the players
		{
			playerNames.push_back(players[i].getUsername());
		}

		GetPlayersInRoomResponse getPlayersInRoomResponse;
		getPlayersInRoomResponse.players = playerNames;
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(getPlayersInRoomResponse);
		requestResult.newHandler = this;
	}
	else
	{
		ErrorResponse errorResponse = { "Non-existing roomID" };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.newHandler = nullptr;
	}

	return requestResult;
}

RequestResult MenuRequestHandler::getStatistics(RequestInfo requestInfo)
{
	RequestResult requestResult;

	std::string username;

	json jsonTranslated = json::from_bson(requestInfo.buffer); //Transforms the buffer's BSON format into a JSON format object
	username = jsonTranslated["username"]; //Takes the username string from the JSON object

	PlayerStatistics playerStatistics = m_handlerFactory->getStatisticsManager().getPlayerStatistics(username);
	GetStatisticsResponse getStatisticsResponse = { 1 };

	getStatisticsResponse.statistics.push_back(std::to_string(playerStatistics.averageAnswerTime));
	getStatisticsResponse.statistics.push_back(std::to_string(playerStatistics.numOfCorrectAnswers));
	getStatisticsResponse.statistics.push_back(std::to_string(playerStatistics.numOfPlayerGames));
	getStatisticsResponse.statistics.push_back(std::to_string(playerStatistics.numOfTotalAnswers));

	requestResult.response = JsonResponsePacketSerializer::serializeResponse(getStatisticsResponse);
	requestResult.newHandler = this;

	return requestResult;
}

RequestResult MenuRequestHandler::getLeaderboard(RequestInfo requestInfo)
{
	RequestResult requestResult;

	std::map<std::string, int> topThreePlayers = m_handlerFactory->getStatisticsManager().getLeaderBoard();

	GetLeaderboardResponse getLeaderboardResponse = { 1 };

	getLeaderboardResponse.topThree = topThreePlayers;

	requestResult.response = JsonResponsePacketSerializer::serializeResponse(getLeaderboardResponse);
	requestResult.newHandler = this;

	return requestResult;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo requestInfo)
{
	RequestResult requestResult;

	int roomID;
	bool roomExists = false;

	json jsonTranslated = json::from_bson(requestInfo.buffer); //Transforms the buffer's BSON format into a JSON format object
	roomID = jsonTranslated["roomId"]; //Takes the roomID string from the JSON object

	std::map<int, Room>& rooms = m_handlerFactory->getRoomManager().getRooms();

	if (rooms.find(roomID) != rooms.end())
	{
		std::string username = jsonTranslated["username"]; //adds the user to the room
		rooms[roomID].addUser(username);
		roomExists = true;
	}

	if (roomExists)
	{
		JoinRoomResponse joinRoomResponse = { 1 };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(joinRoomResponse);
		requestResult.newHandler = this;
	}
	else
	{
		ErrorResponse errorResponse = { "Non-existing roomID" };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.newHandler = nullptr;
	}

	return requestResult;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo requestInfo)
{
	RequestResult requestResult;

	json jsonTranslated = json::from_bson(requestInfo.buffer); //Transforms the buffer's BSON format into a JSON format object

	std::string roomName = jsonTranslated["roomName"];
	unsigned int maxUsers = jsonTranslated["maxUsers"];
	unsigned int questionCount = jsonTranslated["questionCount"];
	unsigned int answerTimeout = jsonTranslated["answerTimeout"]; 
	std::string username = jsonTranslated["username"];

	RoomData roomData;
	roomData.id = 0;
	roomData.maxPlayers = maxUsers;
	roomData.name = roomName;
	roomData.timePerQuestion = answerTimeout;
	roomData.questionCount = questionCount;

	m_handlerFactory->getRoomManager().createRoom(LoggedUser(username), roomData);

	CreateRoomResponse createRoomResponse = { 1 };
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(createRoomResponse);
	requestResult.newHandler = this;

	return requestResult;
}
