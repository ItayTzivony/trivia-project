#pragma once

#include "Codes.h"
#include <vector>

struct DisconnectException : public std::exception
{
	const char* what() const throw ()
	{
		return "Client disconnected/timed out";
	}
};

class Helper
{
public:
	static std::vector<unsigned char> intToBin(int num);
	static int binByteToInt(std::vector<unsigned char> bin);
	static std::vector<unsigned char> fixBufferLen(std::vector<unsigned char> buffer);
};
