#pragma once
#include <ctime>
#include <vector>

struct RequestInfo;

struct RequestResult;

class IRequestHandler
{
private:

public:
	virtual bool isRequestRelevant(RequestInfo requestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo requestInfo) = 0;
};

typedef struct RequestInfo
{
	int reqeustID;
	time_t receivalTime;
	std::vector<unsigned char> buffer;
}RequestInfo;

typedef struct RequestResult
{
	std::vector<unsigned char> response;
	IRequestHandler* newHandler;
}RequestResult;