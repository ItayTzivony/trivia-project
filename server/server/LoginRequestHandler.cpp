#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"

LoginRequestHandler::LoginRequestHandler()
{
	this->m_handlerFactory = new RequestHandlerFactory();
}

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& requestHandle) : m_handlerFactory(&requestHandle)
{
}

LoginRequestHandler::~LoginRequestHandler()
{
	delete this->m_handlerFactory;
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	int code = requestInfo.reqeustID;

	return code == LOGIN_REQUEST_CODE || code == SIGNUP_REQUEST_CODE; //A request is relevant only if it is a login or a signup request
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestResult;
	std::vector<unsigned char> buffer;
	int code = requestInfo.reqeustID;

	if (!isRequestRelevant(requestInfo))
	{
		ErrorResponse errorResponse = { "ERROR: Irrelevant request code" };
		buffer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.newHandler = nullptr;
	}
	else if (code == LOGIN_REQUEST_CODE) //Logs the user into the game
	{
		requestResult = login(requestInfo);
	}
	else if (code == SIGNUP_REQUEST_CODE) //Signups the user
	{
		requestResult = signup(requestInfo);
	}

	return requestResult;
}

RequestResult LoginRequestHandler::login(RequestInfo requestInfo)
{
	RequestResult requestResult;

	std::string username;
	std::string password;

	json jsonTranslated = json::from_bson(requestInfo.buffer); //Transforms the buffer's BSON format into a JSON format object
	
	username = jsonTranslated["username"]; //Takes the username and password strings from the JSON object
	password = jsonTranslated["password"];



	if (m_handlerFactory->getLoginManager().login(username, password)) //Successful login
	{
		LoginResponse loginResponse = { 1 };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(loginResponse);
		requestResult.newHandler = new MenuRequestHandler(*(this->m_handlerFactory));
	}
	else
	{
		ErrorResponse errorResponse = { "ERROR: Failed to login" };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.newHandler = this;
	}

	return requestResult;
}

RequestResult LoginRequestHandler::signup(RequestInfo requestInfo)
{
	RequestResult requestResult;

	std::string username;
	std::string password;
	std::string email;

	json jsonTranslated = json::from_bson(requestInfo.buffer); //Transforms the buffer's BSON format into a JSON format object

	username = jsonTranslated["username"]; //Takes the username, password, and email strings from the JSON object
	password = jsonTranslated["password"];
	email = jsonTranslated["email"];

	if (m_handlerFactory->getLoginManager().signup(username, password, email)) //Successful signup
	{
		SignupResponse signupResponse = { 1 };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(signupResponse);
		requestResult.newHandler = new MenuRequestHandler(*(this->m_handlerFactory));
	}
	else
	{
		ErrorResponse errorResponse = { "ERROR: Username already taken" };
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.newHandler = this;
	}

	return requestResult;
}
