#pragma once
#define  _WINSOCK_DEPRECATED_NO_WARNINGS 
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestHandlerFactory.h"
#include "WSAInitializer.h"
#include <iostream>
#include <ctime>
#include <string>
#include <thread>
#include <map>
#include <vector>

class Communicator
{
private:
	std::map<SOCKET, IRequestHandler*> m_clients;
	SOCKET _serverSocket;
	RequestHandlerFactory m_handlerFactory;


	void bindAndListen(int port);
	void handleNewClient(SOCKET clientSocket);
public:
	Communicator();
	Communicator(RequestHandlerFactory& requestHandle);
	~Communicator();
	void startHandleRequests(int port);
};

