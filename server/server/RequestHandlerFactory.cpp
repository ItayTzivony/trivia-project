#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"

RequestHandlerFactory::RequestHandlerFactory()
{
	this->m_database = new SqliteDatabase();
}

RequestHandlerFactory::RequestHandlerFactory(SqliteDatabase& db) : m_loginManager(db)
{
	this->m_database = &db;
}

LoginRequestHandler RequestHandlerFactory::createLoginRequestHandler()
{
	return LoginRequestHandler();
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}

MenuRequestHandler RequestHandlerFactory::createMenuRequestHanlder()
{
	return MenuRequestHandler();
}

StatisticsManager RequestHandlerFactory::getStatisticsManager()
{
	return this->m_statisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}
