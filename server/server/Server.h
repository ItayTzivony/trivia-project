#pragma once
#include "Communicator.h"
#include "IDatabase.h"
#include "RequestHandlerFactory.h"

class Server
{
private:
	SqliteDatabase m_database;
	RequestHandlerFactory m_handlerFactory;
	Communicator m_communicator;
public:
	Server();
	~Server();
	void run(int port);
};
