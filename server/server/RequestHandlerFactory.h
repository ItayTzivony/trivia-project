#pragma once
#include "SqliteDatabase.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomMemberRequestHandler;

class RequestHandlerFactory
{
private:
	LoginManager m_loginManager;
	RoomManager m_roomManager;
	StatisticsManager m_statisticsManager;

	SqliteDatabase* m_database;
public:
	RequestHandlerFactory();
	RequestHandlerFactory(SqliteDatabase& db);

	LoginRequestHandler createLoginRequestHandler();
	LoginManager& getLoginManager();

	MenuRequestHandler createMenuRequestHanlder();
	StatisticsManager getStatisticsManager();
	RoomManager& getRoomManager();
};
