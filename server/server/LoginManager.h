#pragma once
#include "SqliteDatabase.h"
#include "LoggedUser.h"
#include <vector>
#include <iostream>
#include <mutex>

class LoginManager
{
private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
public:
	LoginManager();
	LoginManager(SqliteDatabase& db);
	~LoginManager();

	bool signup(std::string username, std::string password, std::string email);
	bool login(std::string username, std::string password);
	bool logout(std::string username);
};
