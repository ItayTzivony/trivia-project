#include "Server.h"

Server::Server() : m_database(), m_handlerFactory(m_database), m_communicator(m_handlerFactory)
{
}

Server::~Server()
{

}

void Server::run(int port)
{
	std::string command = "";

	//Starts the server thread, also launches the startHandleReqeusts function
	std::thread t_connector(&Communicator::startHandleRequests, m_communicator, port);
	t_connector.detach();
	
	while (command != "EXIT") //Receives input from the admin until 'EXIT' is inputed
	{
		std::cin >> command;
	}

	t_connector.join();
}
