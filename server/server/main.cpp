#pragma comment(lib, "Ws2_32.lib")
#include "Server.h"

#define PORT 8519

int main()
{
	WSAInitializer wsaInit;
	Server server;
	server.run(PORT);
	return 0;
}
