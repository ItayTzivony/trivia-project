#pragma once
#include "SqliteDatabase.h"

typedef struct PlayerStatistics
{
	float averageAnswerTime;
	int numOfCorrectAnswers;
	int numOfTotalAnswers;
	int numOfPlayerGames;
} PlayerStatistics;

class StatisticsManager
{
private:
	SqliteDatabase* m_database;
public:
	StatisticsManager();
	StatisticsManager(SqliteDatabase& db);

	PlayerStatistics getPlayerStatistics(std::string username);
	std::map<std::string, int> getLeaderBoard();
};
