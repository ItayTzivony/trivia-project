#include "Helper.h"


std::vector<unsigned char> Helper::intToBin(int num)
{
	std::vector<unsigned char> bin;

	for (int i = 0; i < sizeof(int); i++)
	{
		bin.insert(bin.begin(), (unsigned char)num);
		num >>= BYTE_SIZE;
	}

	return bin;
}

int Helper::binByteToInt(std::vector<unsigned char> bin)
{
	int num = (int)bin[0];

	for (int i = 1; i < sizeof(int); i++)
	{
		num >>= BYTE_SIZE;
		num += bin[i];
	}

	return num;
}

std::vector<unsigned char> Helper::fixBufferLen(std::vector<unsigned char> buffer)
{
	std::vector<unsigned char> dataLen(buffer.begin() + CODE_SIZE, buffer.begin() + CODE_SIZE + DATA_LEN);
	std::vector<unsigned char> fixedBuffer(buffer.begin(), buffer.begin() + CODE_SIZE + DATA_LEN + binByteToInt(dataLen));

	return fixedBuffer;
}