#include "JsonRequestPacketDeserializer.h"

LoginRequst JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer)
{
	std::vector<unsigned char> temp(buffer.begin() + CODE_SIZE + DATA_LEN, buffer.end()); //"Cuts" the code and length parts out of the bson
	json j = json::from_bson(temp);
	LoginRequst loginRequest;

	loginRequest.password = j.at("password"); //Takes the values stored under the 'password' and 'username' keys
	loginRequest.username = j.at("username");

	return loginRequest;
}

SignupRequst JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer)
{
	std::vector<unsigned char> temp(buffer.begin() + CODE_SIZE + DATA_LEN, buffer.end()); //"Cuts" the code and length parts out of the bson
	json j = json::from_bson(temp);
	SignupRequst signupRequst;

	signupRequst.password = j.at("password"); //Takes the values stored under the 'password', 'username' and 'email' keys
	signupRequst.username = j.at("username");
	signupRequst.email = j.at("email");

	return signupRequst;
}

GetPlayersInRoomReqeust JsonRequestPacketDeserializer::deserializeGetPlayersRequest(std::vector<unsigned char> buffer)
{
	std::vector<unsigned char> temp(buffer.begin() + CODE_SIZE + DATA_LEN, buffer.end()); //"Cuts" the code and length parts out of the bson
	json j = json::from_bson(temp);
	GetPlayersInRoomReqeust getPlayersReqeust;

	getPlayersReqeust.roomId = j.at("roomID"); //Takes the roomID value

	return getPlayersReqeust;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
	std::vector<unsigned char> temp(buffer.begin() + CODE_SIZE + DATA_LEN, buffer.end()); //"Cuts" the code and length parts out of the bson
	json j = json::from_bson(temp);
	JoinRoomRequest joinRoomRequest;

	joinRoomRequest.roomId = j.at("roomID"); //Takes the roomID value

	return joinRoomRequest;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
	std::vector<unsigned char> temp(buffer.begin() + CODE_SIZE + DATA_LEN, buffer.end()); //"Cuts" the code and length parts out of the bson
	json j = json::from_bson(temp);
	CreateRoomRequest createRoomRequest;

	createRoomRequest.answerTimeout = j.at("timeout"); //Takes the room details that are contained in the json
	createRoomRequest.maxUsers = j.at("maxUsers");
	createRoomRequest.questionCount = j.at("questionCount");
	createRoomRequest.roomName = j.at("roomName");

	return createRoomRequest;
}
