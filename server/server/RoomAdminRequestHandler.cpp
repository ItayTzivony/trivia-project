#include "RoomAdminRequestHandler.h"
#include "RequestHandlerFactory.h"

RoomAdminRequestHandler::RoomAdminRequestHandler()
{
	this->m_handlerFactory = new RequestHandlerFactory();
}

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory& requestHandle) : m_handlerFactory(&requestHandle)
{
}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
	delete this->m_handlerFactory;
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	int code = requestInfo.reqeustID;

	return code == CLOSE_ROOM_REQUEST_CODE || code == START_GAME_REQUEST_CODE || code == GET_ROOM_STATE_REQUEST_CODE;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo requestInfo)
{
	return RequestResult();
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo requestInfo)
{
	return RequestResult();
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo requestInfo)
{
	return RequestResult();
}
