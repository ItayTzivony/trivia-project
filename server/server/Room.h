#pragma once

#include "LoggedUser.h"
#include <vector>

typedef struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	unsigned int questionCount;
	bool isActive;
}RoomData;

class Room
{
private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;
public:
	Room();
	Room(LoggedUser loggedUser, RoomData roomData);
	~Room();
	void addUser(std::string username);
	void removeUser(std::string username);
	std::vector<LoggedUser> getAllUsers();
	RoomData getRoomData();
};