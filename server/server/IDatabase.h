#pragma once
#include <io.h>
#include "sqlite3.h"
#include <string>

class IDatabase
{
private:
	sqlite3* _database;

	virtual bool open() = 0;
	virtual void close() = 0;
public:
	virtual bool doesUserExist(std::string username) = 0;
	virtual bool doesPasswordMatch(std::string password, std::string username) = 0;
	virtual void addNewUser(std::string username, std::string password, std::string email) = 0;
};