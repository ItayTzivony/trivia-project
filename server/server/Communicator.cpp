#include "Communicator.h"

Communicator::Communicator()
{
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

Communicator::Communicator(RequestHandlerFactory& requestHandle) : m_handlerFactory(requestHandle)
{
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

Communicator::~Communicator()
{
	try
	{
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Communicator::bindAndListen(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); //The port that is being listened to
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = inet_addr("127.0.0.1"); //Sets the ip to be the local mechine's ip

	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR) //Connects between the socket and its configuration
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR) //The server starts listening
	{
		throw std::exception(__FUNCTION__ " - listen");
	}

	std::cout << "Waiting for client connection request" << std::endl;
	while (true)
	{
		SOCKET client_socket = ::accept(_serverSocket, NULL, NULL); //Accepts the client and creates a socket for him

		if (client_socket == INVALID_SOCKET)
		{
			throw std::exception(__FUNCTION__);
		}

		std::cout << "Client accepted. Server and client can communicate" << std::endl;

		std::thread clientThread(&Communicator::handleNewClient, this, client_socket); //Creates a thread for the client
		clientThread.detach();

		m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, new LoginRequestHandler(this->m_handlerFactory))); //Adds the client socket and a new instance of LoginRequestHandler to the m_clients map
	}
}

std::string getUsername(std::vector<unsigned char> buffer)
{
	std::string username = "";
	int code = (int)buffer[0]; //Takes the request code out of the request, in order to distinguish between a login and a signup request

	if (code == LOGIN_REQUEST_CODE) //Login request
	{
		LoginRequst loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(buffer);
		username = loginRequest.username;
	}
	else if (code == SIGNUP_REQUEST_CODE) //Signup request
	{
		SignupRequst signupReqeust = JsonRequestPacketDeserializer::deserializeSignupRequest(buffer);
		username = signupReqeust.username;
	}
	return username;
}

void Communicator::handleNewClient(SOCKET clientSocket)
{
	std::string username = "";
	int code;

	DWORD timeout = TIMEOUT_SEC * 1200;
	setsockopt(clientSocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));

	try
	{
		while (true)
		{
			std::vector<unsigned char> buffer(MAX_BUFFER_LEN);
			int bytes = recv(clientSocket, (char*)buffer.data(), buffer.size(), 0);
			if (bytes == SOCKET_ERROR) //Receives a request from the user
			{
				if (WSAGetLastError() == WSAETIMEDOUT)
				{
					throw DisconnectException();
				}
				throw DisconnectException();
			}
			if (bytes < 5)
			{
				throw std::exception(std::string("Client's message is too short(" + std::to_string(bytes) + " bytes)").c_str());
			}
			buffer = Helper::fixBufferLen(buffer); //Matches the buffer's length to the data's length

			RequestInfo requestInfo;

			std::vector<unsigned char> bufferData(buffer.begin() + 5, buffer.end());
			requestInfo.reqeustID = (int)buffer[0];
			requestInfo.receivalTime = std::time(0);
			requestInfo.buffer = bufferData;

			if (username == "")
			{
				username = getUsername(buffer); //Gets the username of the logged or newly registered client
			}
			else
			{
				//adds client's username to the buffer(required for some handlers' functionality)
				json j;
				if (bytes != 5)
				{
					j = json::from_bson(requestInfo.buffer);
				}
				j["username"] = username;
				requestInfo.buffer = json::to_bson(j);
			}
			
			if (!this->m_clients[clientSocket])
			{
				throw std::exception("Next handler was nullptr");
			}
			RequestResult requestResult = this->m_clients[clientSocket]->handleRequest(requestInfo);

			this->m_clients[clientSocket] = requestResult.newHandler;

			if (send(clientSocket, (char*)requestResult.response.data(), requestResult.response.size(), 0) == INVALID_SOCKET)
			{
				throw DisconnectException();
			}
		}
	}
	catch (DisconnectException & error)
	{
		std::cout << error.what() << std::endl;
	}
	catch(std::exception & error)
	{
		std::cout << error.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception" << std::endl;
	}
	if (username != "")
	{
		this->m_handlerFactory.getLoginManager().logout(username); //logs out the user
	}

	this->m_clients.erase(clientSocket);
}

void Communicator::startHandleRequests(int port)
{
	try
	{
		WSAInitializer wsaInit;
		this->bindAndListen(port);
	}
	catch (std::exception & error)
	{
		std::cout << "Error occured: " << error.what() << std::endl;
	}
}
