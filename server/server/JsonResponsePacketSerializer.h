#pragma once

#include "json.hpp"
#include "Helper.h"
#include "Codes.h"
#include "Room.h"
#include <string>

using json = nlohmann::json;

typedef struct ErrorResponse
{
	std::string message;
}ErrorResponse;

typedef struct LoginResponse
{
	unsigned int status;
}LoginResponse;

typedef struct SignupResponse
{
	unsigned int status;
}SignupResponse;

typedef struct LogoutResponse
{
	unsigned int status;
}LogoutResponse;

typedef struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
}GetRoomsResponse;

typedef struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;
}GetPlayersInRoomResponse;

typedef struct GetStatisticsResponse
{
	unsigned int status;
	std::vector<std::string> statistics; //[averageAnswerTime, numOfCorrectAnswers, numOfPlayerGames, numOfTotalAnswers]
}GetStatisticsResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;
}JoinRoomResponse;

typedef struct CreateRoomResponse
{
	unsigned int status;
}CreateRoomResponse;

typedef struct GetLeaderboardResponse
{
	unsigned int status;
	std::map<std::string, int> topThree;
}GetLeaderboardResponse;

typedef struct CloseRoomResponse
{
	unsigned int status;
}CloseRoomResponse;

typedef struct StartGameResponse
{
	unsigned int status;
}StartGameResponse;

typedef struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
}GetGameStateResponse;

typedef struct LeaveRoomResponse
{
	unsigned int status;
}LeaveRoomResponse;

class JsonResponsePacketSerializer
{
public:
	static std::vector<unsigned char> serializeResponse(ErrorResponse errorResponse);
	static std::vector<unsigned char> serializeResponse(LoginResponse loginResponse);
	static std::vector<unsigned char> serializeResponse(SignupResponse signupResponse);

	static std::vector<unsigned char> serializeResponse(LogoutResponse logoutResponse);
	static std::vector<unsigned char> serializeResponse(GetRoomsResponse getRoomsResponse);
	static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse getPlayersInRoomResponse);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse JoinRoomResponse);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse CreateRoomResponse);
	static std::vector<unsigned char> serializeResponse(GetStatisticsResponse GetStatisticsResponse);
	static std::vector<unsigned char> serializeResponse(GetLeaderboardResponse GetLeaderboardResponse);

	static std::vector<unsigned char> serializeResponse(CloseRoomResponse closeRoomResponse);
	static std::vector<unsigned char> serializeResponse(StartGameResponse startGameResponse);
	static std::vector<unsigned char> serializeResponse(GetRoomStateResponse getGameStateResponse);
	static std::vector<unsigned char> serializeResponse(LeaveRoomResponse leaveRoomResponse);
};