#include "SqliteDatabase.h"

std::map<std::string, int> leaderBoard;
 
int getReturnedInt(void* data, int argc, char** argv, char** azColName)
{
	*static_cast<int*>(data) = atoi(argv[0]); //Stores the result of the querry in the referenced variable
	return 0;
}

int getReturnedFloat(void* data, int argc, char** argv, char** azColName)
{
	*static_cast<float*>(data) = atoi(argv[0]); //Stores the result of the querry in the referenced variable
	return 0;
}

int fillQuestionVector(void* data, int argc, char** argv, char** azColName)
{
	((std::vector<std::string>*)data)->push_back(argv[0]); //Pushes the question into the question vector

	return 0;
}

int fillLeaderBoard(void* data, int argc, char** argv, char** azColName)
{
	leaderBoard[argv[0]] = (int)argv[1];

	return 0;
}

bool SqliteDatabase::open()
{
	int doesFileExist = _access(DB_NAME, 0);
	int res = sqlite3_open(DB_NAME, &this->_database); //Opens or creates a database that will be used by the trivia

	if (res != SQLITE_OK) //If there was an error during the opening/creation of the database, sets the value of the database variable to nullptr
	{
		this->_database = nullptr;
		return false;
	}
	if (doesFileExist == -1) //If a new database was just created, creates a 'users' table
	{
		char* errMessage = nullptr;

		std::string sqlStatement = "CREATE TABLE users ("
			"ID INTEGER PRIMARY KEY NOT NULL,"
			"USER_NAME TEXT NOT NULL,"
			"PASSWORD TEXT NOT NULL,"
			"EMAIL TEXT NOT NULL);"; //Creates the 'users' table
		res = sqlite3_exec(this->_database, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

		sqlStatement = "CREATE TABLE statistics ("
			"USER_ID INTEGER PRIMARY KEY NOT NULL,"
			"RANK INTEGER NOT NULL,"
			"EXPERIENCE INTEGER NOT NULL,"
			"AVERAGE_ANSWER_TIME REAL NOT NULL,"
			"CORRECT_ANSWERS INTEGER NOT NULL,"
			"TOTAL_ANSWERS INTEGER NOT NULL,"
			"PLAYED_GAMES INTEGER NOT NULL);"; //Creates the 'statistics' table
		res = sqlite3_exec(this->_database, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	}

	char command[COMMAND_LEN];

	snprintf(command, COMMAND_LEN, "fill_trivia_questions\\fill_trivia_questions.exe %s", DB_NAME);
	system(command);

	return true;
}

void SqliteDatabase::close()
{
	sqlite3_close(this->_database);
	this->_database = nullptr;
}

SqliteDatabase::SqliteDatabase()
{
	open();
}

SqliteDatabase::~SqliteDatabase()
{
	close();
}

bool SqliteDatabase::doesUserExist(std::string username)
{
	int match = 0; //1 if a user with the given name exists, 0 otherwise

	std::string sqlStatement = "SELECT COUNT(*) FROM users WHERE USER_NAME='" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &match, &errMessage);

	return match;
}

bool SqliteDatabase::doesPasswordMatch(std::string password, std::string username)
{
	int match = 0; //1 if the password matches, 0 otherwise

	std::string sqlStatement = "SELECT COUNT(*) FROM users WHERE USER_NAME='" + username + "' AND PASSWORD='" + password + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &match, &errMessage);

	return match;
}

void SqliteDatabase::addNewUser(std::string username, std::string password, std::string email)
{
	std::string sqlStatement = "INSERT INTO users (USER_NAME, PASSWORD, EMAIL) VALUES('" + username + "', '" + password + "', '" + email + "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

	int userID = 0;

	sqlStatement = "SELECT id FROM users WHERE username = " + username + ";";
	res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &userID, &errMessage);

	sqlStatement = "INSERT INTO statistics (USER_ID, RANK, EXPERIENCE, AVERAGE_ANSWER_TIME, CORRECT_ANSWERS, TOTAL_ANSWERS, PLAYED_GAMES) VALUES('" + std::to_string(userID) + "', 1, 0, 0, 0, 0, 0);";
}

std::vector<std::string> SqliteDatabase::getQuestions(int amount)
{
	std::vector<std::string> questionVector;

	char* errMessage = nullptr;
	std::string sqlStatement = "SELECT * FROM questions LIMIT" + std::to_string(amount) + ";";
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &questionVector, &errMessage);
	
	return questionVector;
}

float SqliteDatabase::getPlayerAverageAnswerTime(std::string username)
{
	int userID = 0;

	char* errMessage = nullptr;
	std::string sqlStatement = "SELECT id FROM users WHERE username = " + username + ";";
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &userID, &errMessage);

	float averageAnswerTime = 0;

	sqlStatement = "SELECT AVERAGE_ANSWER_TIME FROM statistics WHERE USER_ID = " + std::to_string(userID) + ";";
	res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedFloat, &averageAnswerTime, &errMessage);

	return averageAnswerTime;
}

int SqliteDatabase::getNumOfCorrectAnswers(std::string username)
{
	int userID = 0;

	char* errMessage = nullptr;
	std::string sqlStatement = "SELECT id FROM users WHERE username = " + username + ";";
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &userID, &errMessage);

	int numOfCorrectAnswers = 0;

	sqlStatement = "SELECT CORRECT_ANSWERS FROM statistics WHERE USER_ID = " + std::to_string(userID) + ";";
	res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &numOfCorrectAnswers, &errMessage);

	return numOfCorrectAnswers;
}

int SqliteDatabase::getNumOfTotalAnswers(std::string username)
{
	int userID = 0;

	char* errMessage = nullptr;
	std::string sqlStatement = "SELECT id FROM users WHERE username = " + username + ";";
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &userID, &errMessage);

	int numOfTotalAnswers = 0;

	sqlStatement = "SELECT TOTAL_ANSWERS FROM statistics WHERE USER_ID = " + std::to_string(userID) + ";";
	res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &numOfTotalAnswers, &errMessage);

	return numOfTotalAnswers;
}

int SqliteDatabase::getNumOfPlayerGames(std::string username)
{
	int userID = 0;

	char* errMessage = nullptr;
	std::string sqlStatement = "SELECT id FROM users WHERE username = " + username + ";";
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &userID, &errMessage);

	int numOfPlayerGames = 0;

	sqlStatement = "SELECT PLAYED_GAMES FROM statistics WHERE USER_ID = " + std::to_string(userID) + ";";
	res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &numOfPlayerGames, &errMessage);

	return numOfPlayerGames;
}

int SqliteDatabase::getPlayerRank(std::string username)
{
	int userID = 0;

	char* errMessage = nullptr;
	std::string sqlStatement = "SELECT id FROM users WHERE username = " + username + ";";
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &userID, &errMessage);

	int rank = 0;

	sqlStatement = "SELECT rank FROM statistics WHERE USER_ID = " + std::to_string(userID) + ";";
	res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &rank, &errMessage);

	return rank;
}

int SqliteDatabase::getPlayerExperience(std::string username)
{
	int userID = 0;

	char* errMessage = nullptr;
	std::string sqlStatement = "SELECT id FROM users WHERE username = " + username + ";";
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &userID, &errMessage);

	int experience = 0;

	sqlStatement = "SELECT experience FROM statistics WHERE USER_ID = " + std::to_string(userID) + ";";
	res = sqlite3_exec(this->_database, sqlStatement.c_str(), getReturnedInt, &experience, &errMessage);

	return experience;
}

std::map<std::string, int> SqliteDatabase::getLeaderBoard()
{
	leaderBoard.clear();

	char* errMessage = nullptr;
	std::string sqlStatement = "SELECT USER_NAME, RANK FROM users INNER JOIN users, statistics WHERE users.ID = statistics.USER_ID ORDER BY statistics.RANK ASC;";
	int res = sqlite3_exec(this->_database, sqlStatement.c_str(), fillLeaderBoard, nullptr, &errMessage);

	return leaderBoard;
}
