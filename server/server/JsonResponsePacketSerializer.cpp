#include "JsonResponsePacketSerializer.h"

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse errorResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["message"] = errorResponse.message;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)ERROR_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse loginResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = loginResponse.status;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)LOGIN_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse signupResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = signupResponse.status;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)SIGNUP_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse logoutResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = logoutResponse.status;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)LOGOUT_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse getRoomsResponse)
{
	json j;
	json rooms = json::array();
	std::vector<unsigned char> buffer, data;
	
	for (int i = 0; i < getRoomsResponse.rooms.size(); i++)
	{
		json temp = {
			{"id", getRoomsResponse.rooms[i].id},
			{"name", getRoomsResponse.rooms[i].name},
			{"maxPlayers", getRoomsResponse.rooms[i].maxPlayers},
			{"timePerQuestion", getRoomsResponse.rooms[i].timePerQuestion},
			{"questionCount", getRoomsResponse.rooms[i].questionCount},
			{"isActive", getRoomsResponse.rooms[i].isActive}
		};
		rooms.push_back(temp);
	}
	
	j["status"] = getRoomsResponse.status;
	j["rooms"] = rooms;

	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)GET_ROOMS_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse getPlayersInRoomResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["players"] = getPlayersInRoomResponse.players;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)GET_PLAYER_IN_ROOM_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse JoinRoomResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = JoinRoomResponse.status;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)JOIN_ROOM_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse CreateRoomResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = CreateRoomResponse.status;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)CREATE_ROOM_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse GetStatisticsResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = GetStatisticsResponse.status;
	j["statistics"] = GetStatisticsResponse.statistics;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)GET_STATISTICS_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetLeaderboardResponse GetLeaderboardResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = GetLeaderboardResponse.status;
	j["topThree"] = GetLeaderboardResponse.topThree;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)GET_LEADERBOARD_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse closeRoomResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = closeRoomResponse.status;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)CLOSE_ROOM_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse startGameResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = startGameResponse.status;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)START_GAME_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse getRoomStateResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = getRoomStateResponse.status;
	j["gameHasBegun"] = getRoomStateResponse.hasGameBegun;
	j["players"] = getRoomStateResponse.players;
	j["questionCount"] = getRoomStateResponse.questionCount;
	j["answerTimeout"] = getRoomStateResponse.answerTimeout;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)GET_ROOM_STATE_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse leaveRoomResponse)
{
	json j;
	std::vector<unsigned char> buffer, data;

	j["status"] = leaveRoomResponse.status;
	data = json::to_bson(j);

	buffer = Helper::intToBin(data.size());
	buffer.insert(buffer.begin(), (unsigned char)LEAVE_ROOM_RESPONSE_CODE);
	buffer.insert(buffer.end(), data.begin(), data.end());

	return buffer;
}
