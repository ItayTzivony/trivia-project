import sys
import sqlite3
import urllib.request
import json

def main():
    conn = sqlite3.connect('%s' % sys.argv[1])
    c = conn.cursor()
    with urllib.request.urlopen("https://opentdb.com/api.php?amount=500&type=multiple") as url:
        dataDict = json.loads(url.read().decode())
    dataList = dataDict['results']

    c.execute("DROP TABLE IF EXISTS questions")
    c.execute("CREATE TABLE questions (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, QUESTION TEXT NOT NULL, CORRECT_ANSWER TEXT NOT NULL, INCORRECT_ANSWER1 TEXT NOT NULL, INCORRECT_ANSWER2 TEXT NOT NULL, INCORRECT_ANSWER3 TEXT NOT NULL);")
    for data in dataList:
        question = data['question']
        correct_answer = data['correct_answer']
        incorrect_answers = data['incorrect_answers']
        c.execute("INSERT INTO questions (QUESTION, CORRECT_ANSWER, INCORRECT_ANSWER1, INCORRECT_ANSWER2, INCORRECT_ANSWER3) VALUES ('%s', '%s', '%s', '%s', '%s');" % (question, correct_answer, incorrect_answers[0], incorrect_answers[1], incorrect_answers[2]))
        conn.commit()

if __name__ == "__main__":
    main()
