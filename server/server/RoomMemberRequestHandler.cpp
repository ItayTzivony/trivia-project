#include "RoomMemberRequestHandler.h"
#include "RequestHandlerFactory.h"

RoomMemberRequestHandler::RoomMemberRequestHandler()
{
	this->m_handlerFactory = new RequestHandlerFactory();
}

RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory& requestHandle) : m_handlerFactory(&requestHandle)
{
}

RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
	delete this->m_handlerFactory;
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	int code = requestInfo.reqeustID;

	return code == LEAVE_ROOM_REQUEST_CODE || code == GET_ROOM_STATE_REQUEST_CODE;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo requestInfo)
{
	return RequestResult();
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo requestInfo)
{
	return RequestResult();
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo requestInfo)
{
	return RequestResult();
}
