#include "Room.h"

Room::Room()
{

}

Room::Room(LoggedUser loggedUser, RoomData roomData) : m_metadata(roomData)
{
	this->m_users.push_back(loggedUser);
	this->m_metadata.isActive = true;
}

Room::~Room()
{

}

void Room::addUser(std::string username)
{
	this->m_users.push_back(LoggedUser(username));
	this->m_metadata.isActive = true;
}

void Room::removeUser(std::string username)
{
	for (auto user = this->m_users.begin(); user != this->m_users.end(); ++user)
	{
		if (user->getUsername() == username)
		{
			this->m_users.erase(user);
		}
	}

	if (this->m_users.empty())
	{
		this->m_metadata.isActive = false;
	}
}

std::vector<LoggedUser> Room::getAllUsers()
{
	return this->m_users;
}

RoomData Room::getRoomData()
{
	return this->m_metadata;
}