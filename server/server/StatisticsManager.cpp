#include "StatisticsManager.h"

StatisticsManager::StatisticsManager()
{
	this->m_database = new SqliteDatabase();
}

StatisticsManager::StatisticsManager(SqliteDatabase& db) : m_database(&db)
{
}

PlayerStatistics StatisticsManager::getPlayerStatistics(std::string username)
{
	PlayerStatistics playerStats{ this->m_database->getPlayerAverageAnswerTime(username), this->m_database->getNumOfCorrectAnswers(username), this->m_database->getNumOfTotalAnswers(username), this->m_database->getNumOfPlayerGames(username) };

	return playerStats;
}

std::map<std::string, int> StatisticsManager::getLeaderBoard()
{
	return this->m_database->getLeaderBoard();
}
