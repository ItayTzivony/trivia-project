#pragma once
#include "IRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "Codes.h"
#include "Helper.h"
#include "LoggedUser.h"

class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
private:
	LoggedUser m_user;
	RequestHandlerFactory* m_handlerFactory;
public:
	MenuRequestHandler();
	MenuRequestHandler(RequestHandlerFactory& requestHandle);
	~MenuRequestHandler();

	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handleRequest(RequestInfo requestInfo);
	RequestResult signout(RequestInfo requestInfo);
	RequestResult getRooms(RequestInfo requestInfo);
	RequestResult getPlayersInRoom(RequestInfo requestInfo);
	RequestResult getStatistics(RequestInfo requestInfo);
	RequestResult getLeaderboard(RequestInfo requestInfo);
	RequestResult joinRoom(RequestInfo requestInfo);
	RequestResult createRoom(RequestInfo requestInfo);
};
