#include "RoomManager.h"

RoomManager::RoomManager()
{

}

RoomManager::~RoomManager()
{

}

void RoomManager::createRoom(LoggedUser loggedUser, RoomData roomData)
{
	int roomID;
	if (this->m_rooms.empty())
	{
		roomID = 1;
	}
	else
	{
		roomID = (--this->m_rooms.end())->first;
	}
	roomData.id = roomID;
	this->m_rooms.insert(std::pair<int, Room>(roomID, Room(loggedUser, roomData)));
}

void RoomManager::deleteRoom(int ID)
{
	this->m_rooms.erase(ID);
}

bool RoomManager::getRoomState(int ID)
{
	return this->m_rooms[ID].getRoomData().isActive;
}

std::map<int, Room>& RoomManager::getRooms()
{
	return this->m_rooms;
}