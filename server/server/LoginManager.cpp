#include "LoginManager.h"

std::mutex mutex;

LoginManager::LoginManager()
{
	this->m_database = new SqliteDatabase();
}

LoginManager::LoginManager(SqliteDatabase& db)
{
	this->m_database = &db;
}

LoginManager::~LoginManager()
{
	delete this->m_database;
}

bool LoginManager::signup(std::string username, std::string password, std::string email)
{
	std::unique_lock<std::mutex> locker(mutex); //"Locks" the m_database and m_loggedUsers variables
	if (!this->m_database->doesUserExist(username))
	{
		this->m_database->addNewUser(username, password, email); //Adds a new user to the database with the given username, password, and email
		this->m_loggedUsers.push_back(LoggedUser(username));
		locker.unlock(); //"Unlocks" the locked variables
		return true;
	}
	locker.unlock(); //"Unlocks" the locked variables
	return false;
}

bool LoginManager::login(std::string username, std::string password)
{
	for (auto user = this->m_loggedUsers.begin(); user != this->m_loggedUsers.end(); user++) //Searches for a user with the same name
	{
		if (user->getUsername() == username) //The user is already logged, and cant login again
		{
			return false;
		}
	}
	std::unique_lock<std::mutex> locker(mutex); //"Locks" the m_loggedUsers variable
	if (this->m_database->doesUserExist(username) && this->m_database->doesPasswordMatch(password, username)) //If the username and password match, adds the user to the logged users vector
	{
		this->m_loggedUsers.push_back(LoggedUser(username));
		return true;
	}
	locker.unlock(); //"Unlocks" the locked variable
	return false;
}

bool LoginManager::logout(std::string username)
{
	std::unique_lock<std::mutex> locker(mutex); //"Locks" the m_loggedUsers variable
	for (auto user = this->m_loggedUsers.begin(); user != this->m_loggedUsers.end(); user++) //Searches for the correct user
	{
		if (user->getUsername() == username) //Removes him from the logged users vector, and stops the function
		{
			user = this->m_loggedUsers.erase(user);
			return true;
		}
	}
	locker.unlock(); //"Unlocks" the locked variable
	return false;
}
