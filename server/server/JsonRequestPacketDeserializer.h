#pragma once

#include "json.hpp"
#include "Helper.h"
#include "Codes.h"
#include <string>
#include <vector>

using json = nlohmann::json;

typedef struct LoginRequst
{
	std::string username;
	std::string password;
} LoginRequst;

typedef struct SignupRequst
{
	std::string username;
	std::string password;
	std::string email;
} SignupRequst;

typedef struct GetPlayersInRoomReqeust
{
	unsigned int roomId;
} GetPlayersInRoomReqeust;

typedef struct JoinRoomRequest
{
	unsigned int roomId;
} JoinRoomRequest;

typedef struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
} CreateRoomRequest;

class JsonRequestPacketDeserializer
{
public:
	static LoginRequst deserializeLoginRequest(std::vector<unsigned char> buffer);
	static SignupRequst deserializeSignupRequest(std::vector<unsigned char> buffer);

	static GetPlayersInRoomReqeust deserializeGetPlayersRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer);
};
