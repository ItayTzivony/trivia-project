#pragma once
#include "IRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "Codes.h"
#include "Helper.h"
#include "Room.h"
#include "LoggedUser.h"

class RequestHandlerFactory;

class RoomMemberRequestHandler : public IRequestHandler
{
private:
	Room m_room;
	LoggedUser m_user;
	RequestHandlerFactory* m_handlerFactory;
public:
	RoomMemberRequestHandler();
	RoomMemberRequestHandler(RequestHandlerFactory& requestHandle);
	~RoomMemberRequestHandler();

	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult handleRequest(RequestInfo requestInfo);
	RequestResult leaveRoom(RequestInfo requestInfo);
	RequestResult getRoomState(RequestInfo requestInfo);
};
