#pragma once
#include "IRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "Codes.h"
#include "Helper.h"
#include "Room.h"
#include "LoggedUser.h"

class RequestHandlerFactory;

class RoomAdminRequestHandler : public IRequestHandler
{
private:
	Room m_room;
	LoggedUser m_user;
	RequestHandlerFactory* m_handlerFactory;
public:
	RoomAdminRequestHandler();
	RoomAdminRequestHandler(RequestHandlerFactory& requestHandle);
	~RoomAdminRequestHandler();

	bool isRequestRelevant(RequestInfo requestInfo);
	RequestResult closeRoom(RequestInfo requestInfo);
	RequestResult startGame(RequestInfo requestInfo);
	RequestResult getRoomState(RequestInfo requestInfo);
};
