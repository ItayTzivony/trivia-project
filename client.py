import socket
import collections
import json
import bson

SERVER_IP = "127.0.0.1"
SERVER_PORT = 8519

def main():
    try:
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connecting to server
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)

        choice = input("Enter 1 for signup or other for login: ")
        if choice == '1':
            email = input("Enter email: ")
        username = input("Enter username: ")
        password = input("Enter password: ")

        if choice == '1':
            #message = bson.dumps({"username": '"' + username + '"', "password": '"' + password + '"', "email": '"' + email + '"'})
            #code = chr(161)
            message = bson.dumps({"username": username, "password": password, "email": email})
            code = chr(161)
        else:
            #message = bson.dumps({"username": '"' + username + '"', "password": '"' + password + '"'})
            #code = chr(160)
            message = bson.dumps({"username": username, "password": password})
            code = chr(160)

        size = chr(len(message))
        null = chr(0)
        sock.send((code + null * 3 + size + message.decode()).encode())

        server_msg = sock.recv(1024)
        #print(server_msg)
        f = open("log.txt", "w")
        f.write(server_msg[5:].decode())

    except Exception as e:
        print(str(e))
    input("Press any key to close connection")
    sock.send("close".encode())
    sock.close()

if __name__ == "__main__":
    main()
